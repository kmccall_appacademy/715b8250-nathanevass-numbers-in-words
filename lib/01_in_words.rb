class Fixnum   

  def in_words
    return "zero" if self == 0
    formatted_arr = []
    format_array(self).each.with_index do |int, i|
    current_words = in_words_by_place(int, i)
    formatted_arr << [current_words] unless current_words == ""
    end
    formatted_arr.reverse.join(" ")
  end 
 
  WORDS = {
    90 => "ninety",   80 => "eighty",   70 => "seventy",
    60 => "sixty",    50 => "fifty",    40 => "forty",
    30 => "thirty",   20 => "twenty",   19 => "nineteen",
    18 => "eighteen", 17 => "seventeen",16 => "sixteen",
    15 => "fifteen",  14 => "fourteen", 13 => "thirteen",
    12 => "twelve",   11 => "eleven",   10 => "ten",
    9  => "nine",     8  => "eight",    7  => "seven",
    6  => "six",      5  => "five",     4  => "four",
    3  => "three",    2  => "two",      1  => "one", 0 => ""
  }
  
  INDEX = {
    0 => "",
    1 => " thousand",
    2 => " million",
    3 => " billion",
    4 => " trillion"
  }
  
  private 
  
  def in_words_by_place(int, place = 0)
    return WORDS[int] + INDEX[place] if int < 21 && int > 0 
    return "" if int == 0 
    
    #hundreds 
    hundreds_digit = int / 100 
    hundreds_string = ""
    hundreds_digit == 0 ? hundreds_string = "" : hundreds_string = WORDS[hundreds_digit] + " hundred"
    
    #tens and ones
    tens_and_ones_string = ""
    tens_and_ones = int % 100 
    tens_digit = int % 100 / 10
    ones_digit = int % 10 
    if tens_and_ones < 21 
      tens_and_ones_string = WORDS[tens_and_ones]
    elsif
      tens_digit != 0 && ones_digit == 0 
      tens_and_ones_string = WORDS[tens_and_ones]
    else 
      tens_and_ones_string = (WORDS[tens_digit * 10] + " " + WORDS[ones_digit]).strip 
    end 
    
    (hundreds_string + " " + tens_and_ones_string).strip + INDEX[place]
  end 
  
  def format_array(n)
    n.to_s.chars.reverse.each_slice(3).map { |a| a.reverse.join("").to_i }
  end 
end 